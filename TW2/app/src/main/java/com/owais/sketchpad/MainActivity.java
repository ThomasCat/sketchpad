package com.owais.sketchpad;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView canvas = findViewById(R.id.canvas);
        TextView blurb = findViewById(R.id.blurb);
        canvas.setText("●");

        TextView coordinates = findViewById(R.id.coordinates);

        final String[] xValue = {"0"};
        final String[] yValue = {"0"};

        final int[] previousXValue = {0};
        final int[] previousYValue = {0};

        SeekBar x = (SeekBar)findViewById(R.id.x);
        x.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int diff = progress - previousXValue[0];
                if(diff > 0){
                    xValue[0] = String.valueOf(progress);
                    coordinates.setText("Coordinates\n________\nX: "+ xValue[0] +"\nY: "+ yValue[0]);
                    canvas.append(" ●");
                } else {
                    String canvasData = canvas.getText().toString();
                    canvas.setText(canvasData.substring(0, canvasData.length() - 1));
                }
                previousXValue[0] = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        SeekBar y = (SeekBar)findViewById(R.id.y);
        y.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int diff = progress - previousYValue[0];
                if(diff > 0){
                    yValue[0] = String.valueOf(progress);
                    coordinates.setText("Coordinates\n________\nX: "+ xValue[0] +"\nY: "+ yValue[0]);
                    canvas.append("\n●");
                } else {
                    String canvasData = canvas.getText().toString();
                    canvas.setText(canvasData.substring(0, canvasData.length() - 2));
                }
                previousYValue[0] = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        Button clear = findViewById(R.id.clear);
        clear.setOnClickListener(v -> {
            xValue[0] = "0";
            yValue[0] = "0";
            x.setProgress(0);
            y.setProgress(0);
            canvas.setText(null);
            canvas.append("●");

            xValue[0] = "0";
            yValue[0] = "0";
            x.setProgress(0);
            y.setProgress(0);
        });

        TextView credits = findViewById(R.id.credits);
        credits.setOnClickListener(v -> {
            Uri uri = Uri.parse("https://owais.codes"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });

        // Part of termwork 1
        Button changeFontSize = findViewById(R.id.changeFontSize);
        Button changeColor = findViewById(R.id.changeColor);

        changeFontSize.setOnClickListener(new View.OnClickListener() {
            float font=30;
            @Override
            public void onClick(View v) {
                blurb.setTextSize(font);
                coordinates.setTextSize(font);
                credits.setTextSize(font);
                clear.setTextSize(font);
                changeFontSize.setTextSize(font);
                changeColor.setTextSize(font);
                canvas.setTextSize(font);

                font = font + 5;
                if (font == 50) font = 18;
            }
        });

        changeColor.setOnClickListener(new View.OnClickListener() {
            int color = 0;
            @Override
            public void onClick(View v) {
                color++;
                switch (color){
                    case 1:
                        blurb.setTextColor(Color.YELLOW);
                        coordinates.setTextColor(Color.YELLOW);
                        credits.setTextColor(Color.YELLOW);
                        clear.setTextColor(Color.YELLOW);
                        changeFontSize.setTextColor(Color.YELLOW);
                        changeColor.setTextColor(Color.YELLOW);
                        canvas.setTextColor(Color.YELLOW);
                        break;

                    case 2:
                        blurb.setTextColor(Color.GREEN);
                        coordinates.setTextColor(Color.GREEN);
                        credits.setTextColor(Color.GREEN);
                        clear.setTextColor(Color.GREEN);
                        changeFontSize.setTextColor(Color.GREEN);
                        changeColor.setTextColor(Color.GREEN);
                        canvas.setTextColor(Color.GREEN);
                        break;

                    case 3:
                        blurb.setTextColor(Color.BLUE);
                        coordinates.setTextColor(Color.BLUE);
                        credits.setTextColor(Color.BLUE);
                        clear.setTextColor(Color.BLUE);
                        changeFontSize.setTextColor(Color.BLUE);
                        changeColor.setTextColor(Color.BLUE);
                        canvas.setTextColor(Color.BLUE);
                        break;

                    case 4:
                        blurb.setTextColor(Color.BLACK);
                        coordinates.setTextColor(Color.BLACK);
                        credits.setTextColor(Color.BLACK);
                        clear.setTextColor(Color.BLACK);
                        changeFontSize.setTextColor(Color.BLACK);
                        changeColor.setTextColor(Color.BLACK);
                        canvas.setTextColor(Color.BLACK);
                        break;

                    case 5:
                        color = 1;
                }
            }
        });
    }
}