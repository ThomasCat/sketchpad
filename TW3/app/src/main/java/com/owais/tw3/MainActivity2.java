package com.owais.tw3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity2 extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    private static final String[] colorSet1 = {"Red", "Blue", "Green"};
    private static final String[] colorSet2 = {"Pink", "Yellow", "Black"};
    private static final String[] colorSet3 = {"Orange", "Indigo", "White"};
    private ArrayList<String> selectedColors = new ArrayList<>();
    TextView selections;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Spinner spinner1 = findViewById(R.id.spinner1);
        ArrayAdapter<String>adapter1 = new ArrayAdapter<>(MainActivity2.this, android.R.layout.simple_spinner_item, colorSet1);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(this);

        Spinner spinner2 = findViewById(R.id.spinner2);
        ArrayAdapter<String>adapter2 = new ArrayAdapter<>(MainActivity2.this, android.R.layout.simple_spinner_item, colorSet2);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(this);

        Spinner spinner3 = findViewById(R.id.spinner3);
        ArrayAdapter<String>adapter3 = new ArrayAdapter<>(MainActivity2.this, android.R.layout.simple_spinner_item, colorSet3);

        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adapter3);
        spinner3.setOnItemSelectedListener(this);

        selections = findViewById(R.id.selections);

        Button clear = findViewById(R.id.clear);
        clear.setOnClickListener(v -> {
            selectedColors.clear();
            selections.setText("Nothing was selected");
        });

        Button playground = findViewById(R.id.playground);
        playground.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        String text = "The colors you selected are";
            for (int colorId = 0; colorId == selectedColors.size(); colorId++) {
                try {
                    text.concat(" "+selectedColors.get(colorId));
                } catch (Exception e) { }
            }

            switch (position) {
                case 0:
                    selectedColors.add(colorSet1[position]);
                    selections.setText(text);
                    for (int colorId = 0; colorId == selectedColors.size(); colorId++) {
                        try {
                            text.concat(" "+selectedColors.get(colorId));
                        } catch (Exception e) { }
                    }
                    break;
                case 1:
                    selectedColors.add(colorSet1[position]);
                    selections.setText(text);
                    for (int colorId = 0; colorId == selectedColors.size(); colorId++) {
                        try {
                            text.concat(" "+selectedColors.get(colorId));
                        } catch (Exception e) { }
                    }
                    break;
                case 2:
                    selectedColors.add(colorSet1[position]);
                    selections.setText(text);
                    for (int colorId = 0; colorId == selectedColors.size(); colorId++) {
                        try {
                            text.concat(" "+selectedColors.get(colorId));
                        } catch (Exception e) { }
                    }
                    break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }

}