package com.owais.tw3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView item = findViewById(R.id.umbrella);

        ImageView leftButton = findViewById(R.id.leftButton);
        ImageView rightButton = findViewById(R.id.rightButton);
        ImageView upButton = findViewById(R.id.upButton);
        ImageView downButton = findViewById(R.id.downButton);
        ImageView rotateLeftButton = findViewById(R.id.rotateLeftButton);
        ImageView rotateRightButton = findViewById(R.id.rotateRightButton);
        ImageView resetButton = findViewById(R.id.resetButton);

        upButton.setOnClickListener(v -> {
            item.animate()
                    .translationY(item.getHeight()*-1)
                    .alpha(1.0f)
                    .setListener(null);
            Toast.makeText(getApplicationContext(), "Name moved up", Toast.LENGTH_LONG).show();
        });

        downButton.setOnClickListener(v -> {
            item.animate()
                    .translationY(item.getHeight()*1)
                    .alpha(1.0f)
                    .setListener(null);
            Toast.makeText(getApplicationContext(), "Name moved down", Toast.LENGTH_LONG).show();
        });

        leftButton.setOnClickListener(v -> {
            item.animate()
                    .translationX(item.getWidth()*-1)
                    .alpha(1.0f)
                    .setListener(null);
            Toast.makeText(getApplicationContext(), "Name moved left", Toast.LENGTH_LONG).show();
        });

        rightButton.setOnClickListener(v -> {
            item.animate()
                    .translationX(item.getWidth()*1)
                    .alpha(1.0f)
                    .setListener(null);
            Toast.makeText(getApplicationContext(), "Name moved right", Toast.LENGTH_LONG).show();
        });

        rotateLeftButton.setOnClickListener(v -> {
            item.animate()
                    .rotation(-90)
                    .alpha(1.0f)
                    .setListener(null);
            Toast.makeText(getApplicationContext(), "Name spun left", Toast.LENGTH_LONG).show();
        });

        rotateRightButton.setOnClickListener(v -> {
            item.animate()
                    .rotation(90)
                    .alpha(1.0f)
                    .setListener(null);
            Toast.makeText(getApplicationContext(), "Name spun right", Toast.LENGTH_LONG).show();
        });

        resetButton.setOnClickListener(v -> {
            item.animate()
                    .rotation(0)
                    .translationX(0)
                    .translationY(0);
            Toast.makeText(getApplicationContext(), "Name reset", Toast.LENGTH_LONG).show();
        });

    }
}